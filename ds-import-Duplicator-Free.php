<?php 
/**
 * Duplicator Free import handler for https://snapcreek.com/duplicator/
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-Duplicator-Free.php');

// Find root via wp-blog-header.php
$root = DS_Utils::find_first_file($details['source'], 'wp-blog-header.php');
if ( $root == '' ) return;
$root = (new GString($root))->delRightMost('wp-blog-header.php')->__toString();

// Find duplicator free wp-config file in root
$wp_config = DS_Utils::find_all_files($root, "dup-wp-config-arc*.txt");
if ( count($wp_config) == 0 ) return;
$wp_config = $wp_config[0];

// Move and rename to wp-config.php in root
rename($wp_config, $root . 'wp-config.php');
$details['wp_config'] = $root . 'wp-config.php';

// Locate database and move to root
$files = DS_Utils::find_all_files($root . 'dup-installer', 'dup-database_*.sql');
if (count($files) == 0) return;
rename($files[0], $root . '/ds_temp.sql');

// Cleanup archive folder
DS_Utils::remove_folder($root . 'dup-installer');
$files = DS_Utils::find_all_files($root, "*_installer-backup.php");
if (count($files) != 0) {
    foreach ($files as $file) {
        unlink($file);
    }
}

// Identify file format and fillout details
$details['format'] = 'Duplicator Free';
