<?php 
/**
 * UpDraftPlus import handler for https://updraftplus.com/
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-UpDraftPlus.php');

// Unzip the database .gz archive first
$files = DS_Utils::find_all_files($details['source'], '*-db.gz');
if (count($files) == 0) return;
$f = (new GString($files[0]))->getRightMost(DIRECTORY_SEPARATOR)->__toString();
$p = (new GString($files[0]))->delRightMost(DIRECTORY_SEPARATOR)->__toString();
$ds_runtime->exec('gzip -d ' . $f, $p);
$f = (new GString($f))->delRightMost('.gz')->__toString();
rename($p . '/' . $f, $p . '/ds_temp.sql');

// Open the database file and extract the WordPress version number in first 15 lines
$fh = fopen($p . '/ds_temp.sql', 'rb');
if ($fh === false) {
    $ds_runtime->debugLog('Could not open file: '. $p . '/ds_temp.sql');
}
$content = '';
for ($i = 0; $i < 15; $i++) {
    // Read a line
    $line = fgets($fh);
    if ($line !== false) {
        $content .= $line;
    } else {
        $ds_runtime->debugLog('An error occurred while reading from file: ' . $p . '/ds_temp.sql');
    }
}
if (fclose($fh) === false) {
    $ds_runtime->debugLog('Could not close file: ' . $p . '/ds_temp.sql');
}
if (false == strpos($content, '# WordPress Version: ')) return;
$wp_version = (new GString($content))->delLeftMost('# WordPress Version: ')->getLeftMost(",")->__toString();
$details['siteRoot'] = (new GString($content))->delLeftMost('# ABSPATH: ')->getLeftMost('#')->trim()->__toString();
$ds_runtime->debugLog('Detected WordPress version ' . $wp_version);

// Download the coordinating WordPress version
$cmd = 'wget -q http://wordpress.org/wordpress-' . $wp_version . '.zip -O wordpress.zip --secure-protocol tlsv1';
$ds_runtime->exec($cmd, $p);
$ds_runtime->exec('unzip -qq wordpress.zip', $p);
$ds_runtime->exec('rm wordpress.zip', $p);

// Move wordpress files into root
DS_Utils::move_folder($p . '/wordpress', $details['source']);
DS_Utils::remove_folder($p . '/wordpress');
rename($p . '/wp-config-sample.php', $p . '/wp-config.php');
$details['wp_config'] = $p . '/wp-config.php';

// Unzip remaining files
$files = DS_Utils::find_all_files($details['source'], 'backup_*s.zip');
foreach ($files as $f) {
    $f = (new GString($f))->getRightMost(DIRECTORY_SEPARATOR)->__toString();
    $ds_runtime->exec('unzip -qq -o -d wp-contents' . $f, $p);
    $ds_runtime->exec('rm ' . $f, $p);
}

// Identify file format and fillout details
$details['format'] = 'UpDraftPlus';
