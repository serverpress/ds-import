<?php
/**
 * DesktopServer 3.9.X import handler
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-DesktopServer3.php');

// Check for database1.sql file and look for iterations
$root = (new GString($details['wp_config']))->delRightMost('wp-config.php')->__toString();
if ($root == "") return; // No wp-config.php found

$files = [];
$i = 0;
while (file_exists($root . 'database' . ($i + 1) . '.sql')) {
    $files[] = 'database' . ($i + 1) . '.sql';
    $i++;
}

// Look for original database.sql in root
if (count($files) == 0) {
    if (file_exists($root . 'database.sql')) {
        $files[] = 'database.sql';
    }
}

// If still no database files found, return
if (count($files) == 0) return;

// Concat all files in ds_temp.sql
$cmd = '';
foreach($files as $key => $sql_file) {
    $cmd .= ' ' . $sql_file;
}
$cmd = 'cat ' . $cmd . ' > ds_temp.sql';
$ds_runtime->exec($cmd, $root);

// Cleanup/remove files
foreach($files as $key => $sql_file) {
    unlink($root . $sql_file);
}

// Move other files into place
DS_Utils::move_folder($root, $details['source']);

// Identify file format and fillout details
$details['format'] = 'DesktopServer 3.9.X';
