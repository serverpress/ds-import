<?php 
/**
 * Duplicator Pro and Free import handler for https://snapcreek.com/duplicator/
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-Duplicator-Pro.php');

// Find root via wp-blog-header.php
$root = DS_Utils::find_first_file($details['source'], 'wp-blog-header.php');
if ( $root == '' ) return;
$root = (new GString($root))->delRightMost('wp-blog-header.php')->__toString();

// Find dup-installer folder
if ( !file_exists($root . 'dup-installer') ) return;
$wp_config = DS_Utils::find_first_file($root . 'dup-installer', 'source_site_wpconfig');

// Check for duplicator free wp-config.php
if ( $wp_config == '' ) {
    $files = DS_Utils::find_all_files($details['source'], 'dup-wp-config-*.txt');
    if ( count($files) == 0 ) return;
    $wp_config = $files[0];
};

// Move and rename to wp-config.php in root
rename($wp_config, $root . 'wp-config.php');
$details['wp_config'] = $root . 'wp-config.php';

// Locate database and move to root
$files = DS_Utils::find_all_files($root . 'dup-installer', 'dup-database_*.sql');
if (count($files) == 0) return;
rename($files[0], $root . '/ds_temp.sql');

// Cleanup archive folder
DS_Utils::remove_folder($root . 'dup-installer');

// Cleanup the installer backup file
$files = DS_Utils::find_all_files($details['source'], '*_installer-backup.php');
if (count($files) != 0) {
    unlink($files[0]);
}

// Identify file format and fillout details
$details['format'] = 'Duplicator Pro';
