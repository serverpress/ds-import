<?php
/**
 * Detect the file format and invoke the appropiate conversion handler.
 */
use Steveorevo\GString as GString;

function ds_detector($details) {
    global $ds_runtime;

    // Find first instance of wp-config within decompressed archive
    $wp_config = DS_Utils::find_first_file($details['source'], 'wp-config.php');
    $wp_config = str_replace("\\", "/", $wp_config);
    $details['wp_config'] = $wp_config;
    $details['format'] = 'unknown';

    /**
     * TODO: Support finding more than one wp-config.php file using find_all_files
     * Perhaps return array of ds_details.json files after decompression using js hook 
     * specific for cPanel
     */


    /**
     * Our list of file import handlers
     */
    $ds_importers = [
        'ds-import-backupbuddy',
        'ds-import-cPanel',
        'ds-import-BackUpWordPress',
        'ds-import-DesktopServer3',
        'ds-import-Duplicator-Pro',
        'ds-import-Duplicator-Free',
        'ds-import-UpdraftPlus',
    ];

    // Try each of our import handler until format is determined
    foreach( $ds_importers as $file ) {
        include($file . '.php');
        if ($details['format'] != 'unknown') break;
    }

    // Detect native file format last
    if (file_exists($details['source'] . '/.ds/ds_details.json')) {
        return $details;
    };
    
    // Throw ds_import_found_format API action
    $details = $ds_runtime->do_action('ds_import_found_format', $details);

    // Create temp database with unique name (to allow future multitasking)
    $dbCreds = DS_Utils::create_db('temp_');
 
    // Look for root ds_temp.sql to import it to temp database
    $file = $details['source'] . '/ds_temp.sql';
    $cmd = getenv('DS_FOLDER') . '/runtime/';
    if ( PHP_OS !== 'Darwin' ) {
        $cmd .= 'win_x64/boot.bat bash -c "';
    }else{
        $cmd .= 'mac_x64/boot.sh ';
    }
    $cmd .= 'mysql --user=' . $dbCreds['dbUser'] . ' --password=';
    if ( PHP_OS !== 'Darwin' ) {
        $cmd .= $dbCreds['dbPass'] . ' ' . $dbCreds['dbName'] . ' < \\"' . $file . '\\"';
    }else{
        // Don't escape quotes on macos
        $cmd .= $dbCreds['dbPass'] . ' ' . $dbCreds['dbName'] . ' < "' . $file . '"';
    }
    $cmd .= ' 2>/dev/null';
    if ( PHP_OS !== 'Darwin' ) {
        $cmd .= '"';
    }
    $ds_runtime->debugLog($cmd);
    $r = shell_exec($cmd);
    if ($r !== NULL) $ds_runtime->debugLog($r);
    @unlink($file);

    // Convert the database to JSON using mysql2json
    $tables = new GString(shell_exec('mysql2json -l ' . $dbCreds['dbName']));
    $tables = $tables->delLeftMost(":")->replace("\r\n", "\n")->trim()->__toString();
    $tables = explode("\n", $tables);

    // Ensure folder exists
    $dbFolder = $details['source'] . "/.ds/database";
    if (!file_exists($dbFolder)) {
        mkdir($dbFolder, 0755, true);
    }

    // Dump each table to the .ds/database folder
    $total = count($tables);
    $bar = null;
    $i = 1;
    foreach($tables as $t){
        $t = trim($t);
        $cmd = 'mysql2json -t ' . $t . ' -o "' . $dbFolder . '/' .  $t . '.json" ' . $dbCreds['dbName'];
        $ds_runtime->debugLog($cmd);
        shell_exec($cmd);
    }

    // Remove, cleanup, database
    $sql = "DROP USER '" . $dbCreds['dbName'] . "'@'localhost';DROP DATABASE IF EXISTS " . $dbCreds['dbName'] . ";";
    DS_Utils::execute_sql($sql);

    // Replace .htaccess with basic one, backup original to .htaccess.bak
    // TODO: make control panel settings for this
    if (file_exists($details['source'] . '/.htaccess')) {
        if (file_exists($details['source'] . '/.htaccess.bak')) {
            @unlink($details['source'] . '/.htaccess.bak');
        }
        rename($details['source'] . '/.htaccess', $details['source'] . '/.htaccess.bak');
    }
    copy(__DIR__ . '/htaccess.sav', $details['source'] . '/.htaccess');

    // Clean up __MACOSX folder
    if (file_exists($details['source'] . '/__MACOSX')) {
        DS_Utils::remove_folder( $details['source'] . '/__MACOSX' );
    }

    // TODO: Disable caching based on control panel settings

    // TODO: Delete transients based on control panel settings

    // TODO: Turn off blog_public based on control panel settings
    // UPDATE $wpdb->prefix . 'options' SET option_value = '0' WHERE option_name = 'blog_public';

    // Cleanup
    unset($details['wp_config']);
    
    return $details;
}
