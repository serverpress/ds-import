<?php
/**
 * Hook into DesktopServer 5 API
 */
global $ds_runtime;
$ds_runtime->add_action('ds_workflow_import_adapter', function($details) {

    // Lean & mean; only include code on import adapter action
    include('ds-detector.php');
    return ds_detector($details);
});