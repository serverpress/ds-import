<?php
/**
 * Plugin Name: DesktopServer Import
 * Plugin URI: https://github.com/serverpress/ds-import
 * Description: Provides import file format support for various backup file formats and hosting provider archives.
 * Version: 0.0.1
 * Author: Stephen J. Carnam
 * Author URI: https://serverpress.com/staff-member/stephen-j-carnam/
 * 
 *  Tags: must-use
 */
