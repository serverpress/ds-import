<?php
/**
 * BackupWordPress import handler from https://wordpress.org/plugins/backupwordpress/
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-BackUpWordPress.php');
$root = (new GString($details['wp_config']))->delRightMost('wp-config.php')->__toString();
if ($root == "") return; // No wp-config.php found

// Check for database-*.sql file
$files = DS_Utils::find_all_files($root, 'database-*.sql');
if (count($files) == 0) return;

// Rename the given database-*.sql file to ds_temp.sql
rename($files[0], $details['source'] . '/ds_temp.sql');

// Identify file format and fillout details
$details['format'] = 'BackupWordPress';