<?php
/**
 * cPanel import handler, check for valid cPanel file contents as defined at:
 * https://docs.cpanel.net/knowledge-base/backup/backup-tarball-contents/84/
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-cPanel.php');

// Check for homedir and mysql directory
if (FALSE == strpos($details['wp_config'], '/homedir/public_html/')) return;
$mysql_folder = (new GString($details['wp_config']))->delRightMost('/homedir/public_html/')->__toString() . '/mysql';
if (! file_exists($mysql_folder)) return;
$ds_runtime->debugLog('Found mysql folder at ' . $mysql_folder);

// Check for homedir_paths file in root
$homedir_paths = (new GString($details['wp_config']))->delRightMost('/homedir/public_html/')->__toString() . '/homedir_paths';
if (file_exists($homedir_paths)) {
    $ds_runtime->debugLog('Found homedir_paths file at ' . $homedir_paths);
    $details['siteRoot'] = trim(file_get_contents($homedir_paths)) . '/public_html';
}

// Check for matching .sql as defined in wp-config.php's DB_NAME
$sql_file = $mysql_folder . '/' . DS_Utils::get_php_define($details['wp_config'], 'DB_NAME') . '.sql';
$ds_runtime->debugLog('Found matching sql file at ' . $sql_file);
if (! file_exists($sql_file)) return;

// Move the sql file to root as ds_temp.sql
$ds_runtime->debugLog("Moving " . $sql_file . " to " . $details['source'] . '/ds_temp.sql');
rename($sql_file, $details['source'] . '/ds_temp.sql');

// Move other files into place
DS_Utils::move_folder((new GString($details['wp_config']))->delRightMost('wp-config.php')->__toString(), $details['source']);

// Cleanup archive folder
DS_Utils::remove_folder((new GString($details['wp_config']))->delRightMost('/homedir/public_html/')->__toString());

// Identify file format and fillout details
$details['format'] = 'cPanel';