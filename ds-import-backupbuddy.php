<?php
/**
 * BackupBuddy import handler, check for valid BackupBuddy file contents.
 * 
 */
use Steveorevo\GString as GString;

global $ds_runtime;
$ds_runtime->debugLog('Running ds-import-backupbuddy.php');

// For BackupBuddy v8.7.4.0+, locate 'backupbuddy_dat.php' file
$root = (new GString($details['wp_config']))->delRightMost('wp-config.php')->__toString();
if ($root == "") return; // No wp-config.php found

$bbfolder = DS_Utils::find_first_file($root, 'backupbuddy_dat.php');
if ( $bbfolder == '' ) return;
$ds_runtime->debugLog('Found BackupBuddy dat file at ' . $bbfolder);

// Gather all sql files in given folder
$bbfolder = (new GString($bbfolder))->delRightMost('backupbuddy_dat.php')->__toString();
$sql_files = DS_Utils::find_all_files($bbfolder, '*.sql');

// Find *_woocommerce_downloadable_product_permissions first, if present
$woo = '';
$cmd = '';
foreach($sql_files as $key => $sql_file) {
    if (strpos($sql_file, 'woocommerce_downloadable_product_permissions') !== FALSE) {
        $woo = (new GString($sql_file))->getRightMost( DIRECTORY_SEPARATOR )->__toString();
        $ds_runtime->debugLog('Found woocommerce_downloadable_product_permissions file at ' . $sql_file);
    }else{
        $cmd .= ' ' . (new GString($sql_file))->getRightMost( DIRECTORY_SEPARATOR )->__toString();
    }
}

// Put the woocommerce_downloadable_product_permissions file first
if ($woo != '') {
    $cmd = 'cat ' . $woo . $cmd . ' > ds_temp.sql';
}else{
    $cmd = 'cat ' . $cmd . ' > ds_temp.sql';
}
$ds_runtime->exec($cmd, $bbfolder);

// Move the sql file to root
rename($bbfolder . 'ds_temp.sql', $details['source'] . DIRECTORY_SEPARATOR . 'ds_temp.sql');

// Purge the backupbuddy folder
$bbfolder = (new GString($bbfolder))->delRightMost('backupbuddy_temp')->__toString() . 'backupbuddy_temp';
DS_Utils::remove_folder($bbfolder);

// Move other files into place (if not same folder)
$r = (new GString($root))->replaceAll('\\', '/')->toLowerCase()->trimRight('/')->__toString();
$s = (new GString($details['source']))->replaceAll('\\', '/')->toLowerCase()->trimRight('/')->__toString();
if ($r != $s) {
    DS_Utils::move_folder($root, $details['source']);
}

// Identify file format and fillout details
$details['format'] = 'BackupBuddy_v8.7.4.0+';
